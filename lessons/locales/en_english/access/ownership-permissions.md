# Ownership Permissions


In addition to modifying permissions on files, you can also modify the group and user ownership of the file as well.

<b>Modify user ownership</b>
```
sudo chown patty myfile
```

This command will set the owner of myfile to patty.

<b>Modify group ownership</b>

```
sudo chgrp whales myfile
```

This command will set the group of myfile to whales.

<b>Modify both user and group ownership at the same time</b>
If you add a colon and groupname after the user you can set both the user and group at the same time.
```
sudo chown patty:whales myfile
```
